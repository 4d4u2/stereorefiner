﻿using Emgu.CV;
using Emgu.CV.Structure;
using StereoRefiner.Utility;
using System;

namespace StereoRefiner.Model
{
    /// <summary>
    /// The combined calibration result
    /// </summary>
    public class CombinedCalibration
    {
        #region Constructor

        /// <summary>
        /// Defines the combined calibration 
        /// </summary>
        /// <param name="calibration1">The first calibration that we are calculating against</param>
        /// <param name="calibration2">The second calibration that we are calculating against</param>
        public CombinedCalibration(Calibration calibration1, Calibration calibration2) 
        {
            var pose = Helpers.GetRelativePose(calibration1.Extrinsics, calibration2.Extrinsics);
            Rotation = Helpers.ExtractRotation(pose); Translation = Helpers.ExtractTranslation(pose);
            
            Camera1 = calibration1.Intrinsics.Clone();
            Camera2 = calibration2.Intrinsics.Clone();
           
            Distortion1 = calibration1.Distortion.Clone();
            Distortion2 = calibration2.Distortion.Clone();
        }

        #endregion

        #region Parameter Getting and Setters

        /// <summary>
        /// Retrieve the parameters
        /// </summary>
        /// <returns>The parameters that we are getting</returns>
        public double[] GetParameters() 
        {
            var tVec = GetTranslationParameters();
            var rVec = new Matrix<double>(3,1); CvInvoke.Rodrigues(Rotation, rVec);
            return new double[] { Camera1.Data[0, 0], Camera1.Data[1, 1], Camera2.Data[0, 0], Camera2.Data[1, 1], rVec.Data[0,0], rVec.Data[1,0], rVec.Data[2,0], tVec.Data[1,0], tVec.Data[2,0] };
        }

        /// <summary>
        /// Update the camera matrix with respect to the parameters
        /// </summary>
        /// <param name="parameters">The parameters we are updating with</param>
        public void Update(double[] parameters) 
        {
            //Camera1.Data[0, 0] = parameters[0];
            //Camera1.Data[1, 1] = parameters[1];
            //Camera2.Data[0, 0] = parameters[2];
            //Camera2.Data[1, 1] = parameters[3];
            SetRotation(parameters[4], parameters[5], parameters[6]);
            SetTranslation(parameters[7], parameters[8]);
        }

        /// <summary>
        /// Set the rotation matrix
        /// </summary>
        /// <param name="rX">The x component</param>
        /// <param name="rY">The y component</param>
        /// <param name="rZ">The z compoennt</param>
        private void SetRotation(double rX, double rY, double rZ)
        {
            var rVec = new Matrix<double>(new double[] { rX, rY, rZ });
            CvInvoke.Rodrigues(rVec, Rotation);
        }

        /// <summary>
        /// Retrieve the translation parameters
        /// </summary>
        /// <returns>The associated translation parameters</returns>
        private Matrix<double> GetTranslationParameters()
        {
            var baseMatrix = new Matrix<double>(new double[] { 1, 0, 0 });
            var transMatrix = Helpers.NormalizeVector(Translation);
            var angle = Math.Acos(Helpers.Dot(baseMatrix, transMatrix));
            var axis = Helpers.Cross(baseMatrix, transMatrix);
            axis = Helpers.NormalizeVector(axis);
            return axis * angle;
        }

        /// <summary>
        /// Set the translation 
        /// </summary>
        /// <param name="rY">The rotation in the y-direction</param>
        /// <param name="rZ">The rotation in the z-direction</param>
        private void SetTranslation(double rY, double rZ)
        {
            var rVec = new Matrix<double>(new double[] { 0, rY, rZ });
            var rot = new Matrix<double>(3,3); CvInvoke.Rodrigues(rVec, rot);
            var sign = Translation.Data[0, 0] > 0 ? 1 : -1;
            var norm = CvInvoke.Norm(Translation) * sign;
            Translation.Data[0, 0] = norm * rot.Data[0, 0];
            Translation.Data[1, 0] = norm * rot.Data[1, 0];
            Translation.Data[2, 0] = norm * rot.Data[2, 0];
        } 

        #endregion

        #region public Properties

        /// <summary>
        /// Retrieve the first camera matrx
        /// </summary>
        public Matrix<double> Camera1 { get; private set; }

        /// <summary>
        /// The second camera matrix
        /// </summary>
        public Matrix<double> Camera2 { get; private set; }

        /// <summary>
        /// The first distortion matrix
        /// </summary>
        public Matrix<double> Distortion1 { get; private set; }

        /// <summary>
        /// The second distortion matrix
        /// </summary>
        public Matrix<double> Distortion2 { get; private set; }

        /// <summary>
        /// The rotation between elements
        /// </summary>
        public Matrix<double> Rotation { get; private set; }

        /// <summary>
        /// The translation between elements
        /// </summary>
        public Matrix<double> Translation { get; private set; }

        #endregion
    }
}