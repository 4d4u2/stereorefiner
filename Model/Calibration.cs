﻿using Emgu.CV;

namespace StereoRefiner.Model
{
    /// <summary>
    /// The calibration details
    /// </summary>
    public class Calibration
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public Calibration() 
        {
            Intrinsics = new Matrix<double>(3, 3);
            Extrinsics = new Matrix<double>(4, 4);
            Distortion = new Matrix<double>(4, 1);
        }

        #endregion

        #region Public Variables

        /// <summary>
        /// The intrinsics of calibration
        /// </summary>
        public Matrix<double> Intrinsics { get; private set; }

        /// <summary>
        /// The extrinsics of calibration
        /// </summary>
        public Matrix<double> Extrinsics { get; private set; }

        /// <summary>
        /// The distortion that we are processing
        /// </summary>
        public Matrix<double> Distortion { get; private set; }

        #endregion
    }
}
