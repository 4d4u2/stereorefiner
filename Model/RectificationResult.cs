﻿using Emgu.CV;

namespace StereoRefiner.Model
{
    /// <summary>
    /// The result of rectification
    /// </summary>
    public class RectificationResult
    {
        #region Constructor

        /// <summary>
        /// Main Construtor
        /// </summary>
        /// <param name="H1">The warping homography for the first rectification</param>
        /// <param name="H2">The warping homography for the second rectification</param>
        public RectificationResult(Matrix<double> H1, Matrix<double> H2) 
        {
            this.H1 = H1; this.H2 = H2;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The homography for warping point due to rectification
        /// </summary>
        public Matrix<double> H1 { get; private set; }

        /// <summary>
        /// The homography for warping point due to rectification
        /// </summary>
        public Matrix<double> H2 { get; private set; }

        #endregion
    }
}
