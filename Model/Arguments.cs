﻿using StereoRefiner.Loader;
using System;
using System.IO;

namespace StereoRefiner.Model
{
    /// <summary>
    /// Defines the arguments associated with the application
    /// </summary>
    public class Arguments
    {
        #region Private Static Constants

        private static readonly string BASE_PATH = @"D:\Trevor\Research\Calibration\Stereo\Process";

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public Arguments() 
        {
            Parameters = ParameterLoader.Load(Path.Combine(BASE_PATH, "Config.xml"));

            PointSetPath1 = BuildPointsPath(Parameters.ActiveSet, 0);
            PointSetPath2 = BuildPointsPath(Parameters.ActiveSet, 1);
            CalibrationPath1 = BuildCalibrationPath(Parameters.ActiveSet, 0);
            CalibrationPath2 = BuildCalibrationPath(Parameters.ActiveSet, 1);
            OutputPath = BuildOutputPath(Parameters.ActiveSet);
        }

        #endregion

        #region Path Builders

        /// <summary>
        /// Build the path associated with the points
        /// </summary>
        /// <param name="activeSet">The active set that we are calculating</param>
        /// <param name="cameraNumber">The number of the camera that we are using</param>
        /// <returns>The path of the point file</returns>
        private string BuildPointsPath(int activeSet, int cameraNumber)
        {
            var folder = Path.Combine(BASE_PATH, "FinalPoints");
            var fileName = string.Format("Set_{0:0000}_Points_{1}.xml", activeSet, cameraNumber);
            return Path.Combine(folder, fileName);
        }

        /// <summary>
        /// Build the calibration to load calibration from
        /// </summary>
        /// <param name="activeSet">The current active set that we are working with</param>
        /// <param name="cameraNumber">The camera that we ae processing</param>
        /// <returns>The resultant path to the calibraiton file</returns>
        private string BuildCalibrationPath(int activeSet, int cameraNumber)
        {
            var folder = Path.Combine(BASE_PATH, "Calibration");
            var fileName = string.Format("Set_{0:0000}_calibration_{1}.xml", activeSet, cameraNumber);
            return Path.Combine(folder, fileName);
        }

        /// <summary>
        /// Construct the output path for saving the path
        /// </summary>
        /// <param name="activeSet">The current active set that we are working with</param>
        /// <returns>The resultant path that we are saving</returns>
        private string BuildOutputPath(int activeSet)
        {
            var folder = Path.Combine(BASE_PATH, "Calibration");
            var fileName = string.Format("Set_{0:0000}_calibration.xml", activeSet);
            return Path.Combine(folder, fileName);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The path to the first point set
        /// </summary>
        public string PointSetPath1 { get; private set; }

        /// <summary>
        /// The path to the second point set
        /// </summary>
        public string PointSetPath2 { get; private set; }

        /// <summary>
        /// Defines the first calibration path
        /// </summary>
        public string CalibrationPath1 { get; private set; }

        /// <summary>
        /// Defines the second calibration path
        /// </summary>
        public string CalibrationPath2 { get; private set; }

        /// <summary>
        /// Defines the parameters associated with the system
        /// </summary>
        public Parameters Parameters { get; private set; }

        /// <summary>
        /// The path to the output of the left frame points
        /// </summary>
        public string OutputPath { get; private set; }

        #endregion
    }
}
