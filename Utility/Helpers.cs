﻿using Emgu.CV;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Defines a set of helper modules for the calculations within this application
    /// </summary>
    public class Helpers
    {
        #region Helper methods

        /// <summary>
        /// Perform a cross product of 2 column vectors
        /// </summary>
        /// <param name="u">The first vector</param>
        /// <param name="v">The seconf vector</param>
        /// <returns>The resultant matrix</returns>
        public static Matrix<double> Cross(Matrix<double> u, Matrix<double> v)
        {
            var result = new Matrix<double>(3, 1);

            result.Data[0, 0] = u.Data[1, 0] * v.Data[2, 0] - u.Data[2, 0] * v.Data[1, 0];
            result.Data[1, 0] = u.Data[2, 0] * v.Data[0, 0] - u.Data[0, 0] * v.Data[2, 0];
            result.Data[2, 0] = u.Data[0, 0] * v.Data[1, 0] - u.Data[1, 0] * v.Data[0, 0];

            return result;
        }

        /// <summary>
        /// find the dot product between two vectors
        /// </summary>
        /// <param name="u">The first matrix</param>
        /// <param name="v">The second matrix</param>
        /// <returns></returns>
        public static double Dot(Matrix<double> u, Matrix<double> v) 
        {
            return (u.Data[0, 0] * v.Data[0, 0]) + (u.Data[1, 0] * v.Data[1, 0]) + (u.Data[2, 0] * v.Data[2, 0]);
        }

        /// <summary>
        /// Calculate a matrix filled with zeros
        /// </summary>
        /// <param name="rows">The rows in the matrix</param>
        /// <param name="columns">The columns within the matrix</param>
        /// <returns>The matrix result that we have returned</returns>
        public static Matrix<double> GetZeroMatrix(int rows, int columns)
        {
            var matrix = new Matrix<double>(rows, columns);
            matrix.SetZero();
            return matrix;
        }

        /// <summary>
        /// Retrieve the associated identity matrix
        /// </summary>
        /// <param name="rows">The rows associated with the matrix</param>
        /// <param name="columns">The columns associated with the matrix</param>
        /// <returns>The resultant matrix</returns>
        public static Matrix<double> GetIdentityMatrix(int rows, int columns)
        {
            var matrix = new Matrix<double>(rows, columns);
            matrix.SetIdentity();
            return matrix;
        }

        /// <summary>
        /// Defines the logic to get a sub matrix
        /// </summary>
        /// <param name="matrix">The initial matrix that we are getting data from</param>
        /// <param name="rowMin">The minimum row that we are working with</param>
        /// <param name="rowMax">The maximum row that we are working with</param>
        /// <param name="columnMin">The minimum column that we are working with</param>
        /// <param name="columnMax">The maximum column that we are working with</param>
        /// <returns>A submatrix of the initial one</returns>
        public static Matrix<double> GetSubMatrix(Matrix<double> matrix, int rowMin, int rowMax, int columnMin, int columnMax)
        {
            var result = new Matrix<double>(rowMax - rowMin, columnMax - columnMin);

            for (var row = 0; row < result.Rows; row++)
            {
                for (var column = 0; column < result.Cols; column++)
                {
                    var value = matrix.Data[row + rowMin, column + columnMin];
                    result.Data[row, column] = value;
                }
            }

            return result;
        }

        /// <summary>
        /// Normalize an associated vector
        /// </summary>
        /// <param name="vector">The vector that we are normalizing</param>
        /// <returns>The resultant mat</returns>
        public static Matrix<double> NormalizeVector(Matrix<double> vector)
        {
            return vector / CvInvoke.Norm(vector);
        }

        /// <summary>
        /// Retrieve the relative pose between the system
        /// </summary>
        /// <param name="pose1">The first intrinsic matrix</param>
        /// <param name="pose2">The second intrinsics matrix</param>
        /// <returns>The resultant relative pose</returns>
        public static Matrix<double> GetRelativePose(Matrix<double> pose1, Matrix<double> pose2)
        {
            var invPose1 = new Matrix<double>(4, 4); CvInvoke.Invert(pose1, invPose1, Emgu.CV.CvEnum.DecompMethod.Svd);
            return pose2 * invPose1;
        }

        /// <summary>
        /// Extract the rotation component from a pose matrix
        /// </summary>
        /// <param name="pose">The pose that we are extracting from</param>
        /// <returns>The rotation that we have extracted</returns>
        public static Matrix<double> ExtractRotation(Matrix<double> pose)
        {
            var result = new Matrix<double>(3, 3);

            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    result.Data[row, column] = pose.Data[row, column];
                }
            }

            return result;
        }

        /// <summary>
        /// Extract the translation component from a pose matrix
        /// </summary>
        /// <param name="pose">The pose that we are extracting from</param>
        /// <returns>The matrix that we are extracting from</returns>
        public static Matrix<double> ExtractTranslation(Matrix<double> pose)
        {
            var result = new Matrix<double>(3, 1);

            result.Data[0, 0] = pose.Data[0, 3];
            result.Data[1, 0] = pose.Data[1, 3];
            result.Data[2, 0] = pose.Data[2, 3];

            return result;
        }

        #endregion
    }
}
