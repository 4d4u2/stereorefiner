﻿using Emgu.CV;
using Emgu.CV.Structure;
using LMDotNet;
using StereoRefiner.Model;
using System;
using System.Collections.Generic;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Defines the functionality to refine the associated calibration
    /// </summary>
    public class Refiner
    {
        #region Private Variables

        private List<MCvPoint2D64f> _points1;
        private List<MCvPoint2D64f> _points2;
        CombinedCalibration _calibration;

        #endregion

        #region Public Entry Point

        /// <summary>
        /// Refine the given calibration
        /// </summary>
        /// <param name="calibration">The calibration that we have</param>
        /// <param name="points1">The first set of points being refined</param>
        /// <param name="points2">The second set of poitns being refined</param>
        public void Refine(CombinedCalibration calibration, List<MCvPoint2D64f> points1, List<MCvPoint2D64f> points2) 
        {
            // Store the incomming variables
            _points1 = points1; _points2 = points2; _calibration = calibration;

            var parameters = _calibration.GetParameters();

            var solver = new LMSolver(epsilon:1e-30);

            var result = solver.Minimize(CostFunction, parameters, _points1.Count);

            calibration.Update(result.OptimizedParameters);
        }

        #endregion

        #region Calculate the cost of the given configuration

        /// <summary>
        /// Calculate the cost of the given parametrs
        /// </summary>
        /// <param name="parameters">The parameters we are refining</param>
        /// <param name="errors">The errors associated with that cost</param>
        private void CostFunction(double[] parameters, double[] errors) 
        {
            _calibration.Update(parameters);

            var rectification = Rectifier.Find(_calibration);
            var F = rectification.H2.Transpose() * GetU() * rectification.H1;

            double total = 0;
            for (int i = 0; i < _points1.Count; i++)
            {
                errors[i] = GetSampsonError(i, F, _calibration);
                total += errors[i];
            }
            //Console.WriteLine("Total Error: {0}", total);
        }

        /// <summary>
        /// Retrieve the sampson error associated with a given point
        /// </summary>
        /// <param name="i">The first point</param>
        /// <param name="F">The fundametnal matrix</param>
        /// <param name="calibratiion">The associated calibration</param>
        /// <returns>The result</returns>
        private double GetSampsonError(int i, Matrix<double> F, CombinedCalibration calibratiion)
        {
            var pointL = new Matrix<double>(new double[] { _points1[i].X, _points1[i].Y, 1 });
            var pointR = new Matrix<double>(new double[] { _points2[i].X, _points2[i].Y, 1 });

            var fml = F * pointL; var mrTF = pointR.Transpose() * F;

            var top = pointR.Transpose() * F * pointL;
            var topSquared = top.Data[0, 0] * top.Data[0, 0];

            var weight = GetPart(fml, 0, 0) + GetPart(fml, 1, 0) + GetPart(mrTF, 0, 0) + GetPart(mrTF, 0, 1);

            return weight != 0 ? topSquared / weight : 0;
        }

        /// <summary>
        /// Retrieve the associated component
        /// </summary>
        /// <param name="matrix">The matrix that we are getting the data from</param>
        /// <param name="cX">The component of interest</param>
        /// <param name="cY">The compontent of interst in the y</param>
        /// <returns>The result</returns>
        private double GetPart(Matrix<double> matrix, int cX, int cY)
        {
            var part = matrix.Data[cX, cY];
            return part * part;
        }

        /// <summary>
        /// Retrieve the associated u matrix
        /// </summary>
        /// <returns>The U matrix</returns>
        private Matrix<double> GetU()
        {
            var result = new Matrix<double>(3, 3);

            result.Data[0, 0] = 0;
            result.Data[0, 1] = 0;
            result.Data[0, 2] = 0;

            result.Data[1, 0] = 0;
            result.Data[1, 1] = 0;
            result.Data[1, 2] = -1;

            result.Data[2, 0] = 0;
            result.Data[2, 1] = 1;
            result.Data[2, 2] = 0;

            return result;
        }

        #endregion
    }
}
