﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Collections.Generic;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Defines the logic to warp a given set of points
    /// </summary>
    public class PointWarper
    {
        #region Warp Logic

        /// <summary>
        /// Defines the logic to warp a set of points
        /// </summary>
        /// <param name="transform">The transform that we are using to warp a set of points</param>
        /// <param name="points">The points that we are warping</param>
        /// <returns>The list of warped points</returns>
        public static List<MCvPoint2D64f> Warp(Matrix<double> transform, List<MCvPoint2D64f> points) 
        {
            var result = new List<MCvPoint2D64f>();

            foreach (var point in points) 
            {
                var X = transform.Data[0, 0] * point.X + transform.Data[0, 1] * point.Y + transform.Data[0, 2];
                var Y = transform.Data[1, 0] * point.X + transform.Data[1, 1] * point.Y + transform.Data[1, 2];
                var Z = transform.Data[2, 0] * point.X + transform.Data[2, 1] * point.Y + transform.Data[2, 2];

                var newPoint = new MCvPoint2D64f(X / Z, Y / Z);
                result.Add(newPoint);            
            }

            return result;
        }

        #endregion
    }
}
