﻿using Emgu.CV;
using StereoRefiner.Model;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Write the result to disk
    /// </summary>
    public class ResultWriter
    {
        #region Writer Logic

        /// <summary>
        /// Write the result to disk
        /// </summary>
        /// <param name="path">The path that we are writing</param>
        /// <param name="calibration">The combined calibraiton between the two systems</param>
        public static void Write(string path, CombinedCalibration calibration) 
        {
            using (var writer = new FileStorage(path, FileStorage.Mode.FormatXml | FileStorage.Mode.Write)) 
            {
                writer.Write(calibration.Camera1.Mat, "Camera1");
                writer.Write(calibration.Camera2.Mat, "Camera2");
                writer.Write(calibration.Distortion1.Mat, "Distortion1");
                writer.Write(calibration.Distortion2.Mat, "Distortion2");
                writer.Write(calibration.Rotation.Mat, "Rotation");
                writer.Write(calibration.Translation.Mat, "Translation");
            }
        }

        #endregion
    }
}
