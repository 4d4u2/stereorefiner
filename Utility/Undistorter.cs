﻿using StereoRefiner.Model;
using System.Collections.Generic;
using TsaiEngine.Utilities;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Removes distortion a set of points
    /// </summary>
    public class Undistorter
    {
        #region Logic Entry Point

        /// <summary>
        /// The processing logic to undistort feature points in the system
        /// </summary>
        /// <param name="calibration">The calibration detials</param>
        /// <param name="points">The points that we are undistorting</param>
        /// <returns>The resultant list of process</returns>
        public static List<GridPoint> Process(Calibration calibration, List<GridPoint> points) 
        {
            var result = new List<GridPoint>();
            var distortion = new ZhangDistorter(calibration.Intrinsics);

            foreach (var point in points) 
            {
                var imagePoint = distortion.UnDistort(calibration.Distortion, point.ImagePoint);
                var newPoint = new GridPoint(point.ScenePoint, imagePoint);
                result.Add(newPoint);
            }

            return result;
        } 

        #endregion
    }
}
