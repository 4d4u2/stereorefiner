﻿using Emgu.CV;
using Emgu.CV.Structure;
using LMDotNet;

namespace TsaiEngine.Utilities
{
    /// <summary>
    /// Defines the logic to perform Zhang Distortion
    /// </summary>
    public class ZhangDistorter
    {
        #region Private Variables

        private Matrix<double> _cameraMatrix;
        private Matrix<double> _distortion;
        private MCvPoint2D64f _expected;

        #endregion

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="cameraMatrix">The camera matrix associated with the application</param>
        public ZhangDistorter(Matrix<double> cameraMatrix) 
        {
            _cameraMatrix = cameraMatrix;
        }

        #endregion

        #region Add Distortion

        /// <summary>
        /// Defines the logic to distort a given point
        /// </summary>
        /// <param name="distortion">The distoriton parameters that we are working with</param>
        /// <param name="point">The point that we are distorting</param>
        /// <returns>The distorted location of the point</returns>
        public MCvPoint2D64f Distort(Matrix<double> distortion, MCvPoint2D64f point)
        {
            var screen = Image2Screen(point);
            var r2 = screen.X * screen.X + screen.Y * screen.Y; var r4 = r2 * r2;

            var k1 = distortion.Data[0, 0];  var k2 = distortion.Data[1, 0]; var p1 = distortion.Data[2, 0]; var p2 = distortion.Data[3, 0];
            var xu = screen.X; var yu = screen.Y;

            var xd = xu * (1 + k1 * r2 + k2 * r4) + p2 * (r2 + 2 * xu * xu) + 2 * p1 * xu * yu;
            var yd = yu * (1 + k1 * r2 + k2 * r4) + p1 * (r2 + 2 * yu * yu) + 2 * p2 * xu * yu;

            return Screen2Image(new MCvPoint2D64f(xd,yd));
        }

        #endregion

        #region Remove Distortion

        /// <summary>
        /// Defines the logic to undistort a given point
        /// </summary>
        /// <param name="point">The point that we are undistorting</param>
        /// <returns>The undistorted location of the given point</returns>
        public MCvPoint2D64f UnDistort(Matrix<double> distortion, MCvPoint2D64f point)
        {
            _expected = point; _distortion = distortion;
            var solver = new LMSolver(1e-20, 1e-20, 1e-20, 1e-20, 100, 500);
            var result = solver.Solve(CostFunction, new double[] { _expected.X, _expected.Y });
            return new MCvPoint2D64f(result.OptimizedParameters[0], result.OptimizedParameters[1]);
        }

        /// <summary>
        /// The function that we are minimizing
        /// </summary>
        /// <param name="parameters">The parameters of the function</param>
        /// <param name="residuals">The associated residuals</param>
        public void CostFunction(double[] parameters, double[] residuals)
        {
            var point = new MCvPoint2D64f(parameters[0], parameters[1]);
            var solution = Distort(_distortion, point);
            residuals[0] = (_expected.X - solution.X) * (_expected.X - solution.X);
            residuals[1] = (_expected.Y - solution.Y) * (_expected.Y - solution.Y);
        }

        #endregion

        #region Conversion Utilities

        /// <summary>
        /// Convert a screen coordinate to an image coordinate
        /// </summary>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private MCvPoint2D64f Screen2Image(MCvPoint2D64f point)
        {
            var x = point.X * _cameraMatrix.Data[0,0] + _cameraMatrix.Data[0, 2];
            var y = point.Y * _cameraMatrix.Data[1,1] + _cameraMatrix.Data[1, 2];
            return new MCvPoint2D64f(x, y);
        }

        /// <summary>
        /// Convert an image coordinate to a screen coordinate
        /// </summary>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private MCvPoint2D64f Image2Screen(MCvPoint2D64f point)
        {
            var x = (point.X - _cameraMatrix.Data[0, 2]) / _cameraMatrix.Data[0,0];
            var y = (point.Y - _cameraMatrix.Data[1, 2]) / _cameraMatrix.Data[1,1];
            return new MCvPoint2D64f(x, y);
        }

        #endregion
    }
}