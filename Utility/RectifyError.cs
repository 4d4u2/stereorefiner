﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Defines the logic to find the rectification error within left and right points
    /// </summary>
    public class RectifyError
    {
        #region Find error

        /// <summary>
        /// Find the error associated with the given set of points
        /// </summary>
        /// <param name="points1">The first list of points</param>
        /// <param name="points2">The second list of points</param>
        /// <returns>The associated score that was found</returns>
        public static MCvScalar Find(List<MCvPoint2D64f> points1, List<MCvPoint2D64f> points2) 
        {
            var errors = new double[points1.Count];
            for (int i = 0; i < points1.Count; i++) errors[i] = Math.Abs(points1[i].Y - points2[i].Y);
            var input = new Matrix<double>(errors.ToArray());
            var mean = new Matrix<double>(1, 1); var stddev = new Matrix<double>(1, 1);
            CvInvoke.MeanStdDev(input, mean, stddev);
            return new MCvScalar(mean.Data[0, 0], stddev.Data[0, 0]);
        }

        #endregion
    }
}
