﻿using Emgu.CV;
using Emgu.CV.Structure;
using StereoRefiner.Model;

namespace StereoRefiner.Utility
{
    /// <summary>
    /// Get the rectifier images
    /// </summary>
    public class Rectifier
    {
        #region Rectification Logic Entry Point

        /// <summary>
        /// Perform rectifiation based on the calibration results and the current frame
        /// </summary>
        /// <param name="calibration">The associated calibration result</param>
        /// <returns>The resultant rectification</returns>
        public static RectificationResult Find(CombinedCalibration calibration)
        {
            var po1 = GetProjectionMatrix(calibration.Camera1, Helpers.GetIdentityMatrix(3, 3), Helpers.GetZeroMatrix(3, 1));
            var po2 = GetProjectionMatrix(calibration.Camera2, calibration.Rotation, calibration.Translation);
            var c1 = GetCentre(po1); var c2 = GetCentre(po2);
            var rotation = CalculateRotation(c1, c2); var intrinsic = (calibration.Camera1 + calibration.Camera2) / 2.0;
            var pn1 = GetProjectionMatrix(intrinsic, rotation, c1);
            var pn2 = GetProjectionMatrix(intrinsic, rotation, c2);
            return BuildResult(po1, po2, pn1, pn2);
        }

        #endregion

        #region Calculate Projection Matrix

        /// <summary>
        /// Derive a projection matrix
        /// </summary>
        /// <param name="intrinsic">The intrinsic matrix</param>
        /// <param name="rotation">The rotation matrix</param>
        /// <param name="translation">The translation matrix</param>
        /// <returns>The resultant projection matrix</returns>
        private static Matrix<double> GetProjectionMatrix(Matrix<double> intrinsic, Matrix<double> rotation, Matrix<double> translation) 
        {
            var extrinsic = rotation.ConcateHorizontal(translation);
            return intrinsic * extrinsic;
        }

        #endregion

        #region Calculate the associated matrix centre

        /// <summary>
        /// Retrieve the associated matrix
        /// </summary>
        /// <param name="projection">The associated projection matrix</param>
        /// <returns>The resultant centre as a matrix</returns>
        private static Matrix<double> GetCentre(Matrix<double> projection)
        {
            var Q = Helpers.GetSubMatrix(projection, 0, 3, 0, 3);
            var q = Helpers.GetSubMatrix(projection, 0, 3, 3, 4);
            var invQ = new Matrix<double>(3, 3); CvInvoke.Invert(Q, invQ, Emgu.CV.CvEnum.DecompMethod.Cholesky);
            return -1 * invQ * q;
        }

        #endregion

        #region Add logic to build the rotation Matrix

        /// <summary>
        /// Calculate a rotation matrix
        /// </summary>
        /// <param name="center1">The first centre</param>
        /// <param name="center2">The second centre</param>
        /// <returns></returns>
        private static Matrix<double> CalculateRotation(Matrix<double> center1, Matrix<double> center2)
        {
            var v1 = center2 - center1;
            var Z = new Matrix<double>(new double[3,1] {{0},{0},{1}});
            var v2 = Helpers.Cross(Z, v1);
            var v3 = Helpers.Cross(v1, v2);
            v1 = Helpers.NormalizeVector(v1); v2 = Helpers.NormalizeVector(v2); v3 = Helpers.NormalizeVector(v3);
            var result = v1.ConcateHorizontal(v2).ConcateHorizontal(v3);
            return result.Transpose();
        }

        #endregion

        #region Defines the logic to build the associated result

        /// <summary>
        /// Assemble the result from the original poses 
        /// </summary>
        /// <param name="po1">The old projection matrix 1</param>
        /// <param name="po2">The old projection matrix 2</param>
        /// <param name="pn1">The new projection matrix 1</param>
        /// <param name="pn2">The new projection matrix 2</param>
        /// <returns>The resultant rectification</returns>
        private static RectificationResult BuildResult(Matrix<double> po1, Matrix<double> po2, Matrix<double> pn1, Matrix<double> pn2)
        {
            var h1 = GetHomography(po1, pn1); var h2 = GetHomography(po2, pn2);
            return new RectificationResult(h1, h2);
        }

        /// <summary>
        /// Calculate the associated homography
        /// </summary>
        /// <param name="oldProjection">The old projection matrix</param>
        /// <param name="newProjection">The new projection matrix</param>
        /// <returns>The homography that we are dealing with</returns>
        private static Matrix<double> GetHomography(Matrix<double> oldProjection, Matrix<double> newProjection)
        {
            var oldMatrix = Helpers.GetSubMatrix(oldProjection, 0, 3, 0, 3);
            var newMatrix = Helpers.GetSubMatrix(newProjection, 0, 3, 0, 3);
            var invPold = new Matrix<double>(3,3); CvInvoke.Invert(oldMatrix, invPold, Emgu.CV.CvEnum.DecompMethod.LU);
            return newMatrix * invPold;
        }

        #endregion
    }
}
