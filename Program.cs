﻿using StereoRefiner.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StereoRefiner
{
    /// <summary>
    ///  Application Entry Point
    /// </summary>
    public class Program
    {
        #region Entry Point Method

        /// <summary>
        /// Application Entry Point
        /// </summary>
        public static void Main()
        {
            try
            {
                new MainController().Run(); Pause();
            }
            catch (Exception exception) 
            {
                Console.WriteLine("Error: {0}", exception.Message); Pause();
            }
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Defines the logic to pause the screen
        /// </summary>
        private static void Pause()
        {
            Console.WriteLine(); Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
        }

        #endregion
    }
}