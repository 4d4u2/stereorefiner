﻿using StereoRefiner.Loader;
using StereoRefiner.Model;
using StereoRefiner.Utility;
using System;
using System.Collections.Generic;

namespace StereoRefiner.Controller
{
    /// <summary>
    /// Defines the main controller associated with the system
    /// </summary>
    public class MainController
    {
        #region Private Variables

        private Arguments _arguments;
        private Calibration _calibration1;
        private Calibration _calibration2;
        private List<GridPoint> _points1;
        private List<GridPoint> _points2;

        #endregion

        #region Main Constructors

        /// <summary>
        /// Main Controller
        /// </summary>
        public MainController() 
        {
            _arguments = new Arguments();

            var points1 = PointLoader.Load(_arguments.PointSetPath1);
            var points2 = PointLoader.Load(_arguments.PointSetPath2);

            _calibration1 = CalibrationLoader.Load(_arguments.CalibrationPath1);
            _calibration2 = CalibrationLoader.Load(_arguments.CalibrationPath2);

            _points1 = Undistorter.Process(_calibration1, points1);
            _points2 = Undistorter.Process(_calibration2, points2);
        }

        #endregion

        #region Entry Point method

        /// <summary>
        /// Defines the entry execution logic
        /// </summary>
        public void Run()
        {
            // Extract the points
            var image1 = _points1.ConvertAll(point => point.ImagePoint);
            var image2 = _points2.ConvertAll(point => point.ImagePoint);

            // Show the initial error in the points
            var error1 = RectifyError.Find(image1, image2);
            Console.WriteLine("Initial Error: {0} ± {1}", error1.V0, error1.V1);

            // Perform rectification
            var combinedCalibration = new CombinedCalibration(_calibration1, _calibration2);

            var parameters = combinedCalibration.GetParameters();
            combinedCalibration.Update(parameters);
            
            var rectification = Rectifier.Find(combinedCalibration);
            var rImage1 = PointWarper.Warp(rectification.H1, image1);
            var rImage2 = PointWarper.Warp(rectification.H2, image2);
            var error2 = RectifyError.Find(rImage1, rImage2);
            Console.WriteLine("Rectify Error: {0} ± {1}", error2.V0, error2.V1);

            // Refine the calibration
            Console.WriteLine("Attempting to optimize....");
            new Refiner().Refine(combinedCalibration, image1, image2);
            rectification = Rectifier.Find(combinedCalibration);
            rImage1 = PointWarper.Warp(rectification.H1, image1);
            rImage2 = PointWarper.Warp(rectification.H2, image2);
            var error3 = RectifyError.Find(rImage1, rImage2);
            Console.WriteLine("Error after Optimization: {0} ± {1}", error3.V0, error3.V1);

            // Write the result to disk
            Console.WriteLine("Writing the result to disk....");
            ResultWriter.Write(_arguments.OutputPath, combinedCalibration);
        }

        #endregion
    }
}
