﻿using Emgu.CV.Structure;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using StereoRefiner.Model;

namespace StereoRefiner.Loader
{   
    /// <summary>
    /// Load the points from file
    /// </summary>
    public class PointLoader
    {
        #region Entry point 

        /// <summary>
        /// Load feature points that we are dealing with
        /// </summary>
        /// <param name="path">The path that we are dealing with</param>
        /// <returns>The features that we are loading</returns>
        public static List<GridPoint> Load(string path) 
        {
            var result = new List<GridPoint>();

            using (var reader = new StreamReader(path)) 
            {
                var line = reader.ReadLine();

                while (!string.IsNullOrWhiteSpace(line)) 
                {
                    var parts = line.Split(" ".ToArray());
                    var X = double.Parse(parts[0]);
                    var Y = double.Parse(parts[1]);
                    var Z = double.Parse(parts[2]);
                    var u = double.Parse(parts[3]);
                    var v = double.Parse(parts[4]);
                    result.Add(new GridPoint(new MCvPoint3D64f(X, Y, Z), new MCvPoint2D64f(u,v)));

                    line = reader.ReadLine();
                }
            }

            return result;
        }

        #endregion
    }
}