﻿using Emgu.CV;
using StereoRefiner.Model;

namespace StereoRefiner.Loader
{
    /// <summary>
    /// Loads calibration details from a disk
    /// </summary>
    public class CalibrationLoader
    {
        #region The load Logic

        /// <summary>
        /// Loading functionality
        /// </summary>
        /// <param name="path">The path where the calibration</param>
        /// <returns>The calibration details that we are loading</returns>
        public static Calibration Load(string path) 
        {
            var calibration = new Calibration();

            using (var reader = new FileStorage(path, FileStorage.Mode.FormatXml | FileStorage.Mode.Read)) 
            {
                reader.GetNode("Intrinsics").ReadMat(calibration.Intrinsics.Mat);
                reader.GetNode("Extrinsics").ReadMat(calibration.Extrinsics.Mat);
                reader.GetNode("Distortion").ReadMat(calibration.Distortion.Mat);            
            }

            return calibration;
        }

        #endregion
    }
}
